TCGA-CZ-5462
PATHOLOGIC DIAGNOSIS: 
SPECIMEN DESIGNATED "RIGHT KIDNEY" :
RENAL CELL CARCINOMA, clear cell type with focal sarcomatoid
areas, Fuhrman nuclear grade III/IV (4.5 cm).
The tumor abuts but not does not extend through the renal capsule.
The tumor shows extensive necrosis.
No lymphovascular invasion is seen.
The ureter, renal artery, and renal vein resection margins are
negative for tumor.
The non-neoplastic kidney will be evaluated by Renal Pathology, and the
findings will be reported in an addendum.
The AJCC Classification(6th Edition): T1b Nx M1 