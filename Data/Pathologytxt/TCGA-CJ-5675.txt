TCGA-CJ-5675
DIAGNOSIS
(A) RIGHT KIDNEY AND ADRENAL GLAND:
RENAL CELL CARCINOMA (10.0 CM MAXIMUM DIMENSION), CONVENTIONAL TYPE
(70% CLEAR
CELLS, 30% EOSINOPHILIC CELLS), FUHRMAN NUCLEAR GRADE 3,
CONFINED TO THE KIDNEY.
(SEE COMMENT).
Margins of resection free of tumor.
Adrenal gland, no tumor present.
(B) GONADAL VEIN RIGHT:
Blood vessels and fibroadipose tissue, no tumor present.
COMMENT
The renal cell carcinoma in the right kidney appears to be confined to
the kidney and does not invade into the perinephric adipose tissue or
the renal vein. The tumor pushes against the renal sinus but there is
no unequivocal invasion into the renal sinus adipose tissue. Multiple
additional deeper sections were also examined.
