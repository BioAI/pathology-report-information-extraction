TCGA-KL-8338

DIAGNOSIS:
1. SP: kidney, left. radical nephrectomy
Tumor Type:
Renal cell carcinoma - Chromophobe type
Tumor Size:
Greatest diameter is 12.5 cm.
local Invasion (for renal cortical types) :
Involves renal sinus fat
Renal Vein Invasion:
Not identified
Vessels with muscular wall are involved by tumor in the renal sinus
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Unremarkable
Adrenal Gland:
Not identified
Lymph Nodes:
Not identified
Staging for renal cell carcinoma/oncocytoma:
pT3a Tumor invades the adrenal gland or perinephric tissues but not beyond Gerota's fascia
