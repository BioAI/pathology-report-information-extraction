TCGA-B9-5156
Diagnosis: Left kidney, partial laparoscopic nephrectomy 
Histologic tumor type/subtype: renal cell carcinoma, papillary subtype 
Histologic grade (if applicable): Fuhnnan grade 3 of 4 
Tumor size (greatest dimension): 1.7 cm 
Extent of tumor invasion:
Capsular invasion/perirenal adipose tissue: Tumor invades into renal capsule; perirenal fat negative for carcinoma.
Gerota' s fascia: negative for carcinoma
Renal vein: not applicable
Ureter: not applicable
Venous (large vessel): not identified
Lymphatic (small vessel): not identified 
Histologic assessment of surgical margins:
Perirenal adipose tissue: negative for malignancy
Gerota' s fascia: negative for malignancy 
Adrenal gland: not present 
Lymph nodes: none identified 
Other significant findings: - Tumor is 0.5 em from the surgical margin.
- Mild glomemlosclerosis and arteriosclerosis.
- Benign simple cyst. 
AJCC Staging: pT1a pNX