TCGA-A3-3326
DIAGNOSIS BASED ON GROSS AND MICROSCOPIC EXAMINATION:
KIDNEY: Nephrectomy
SPECIMEN TYPE: Radical nephrectomy.
LATERALITY: Right.
TUMOR dITE: Lower pole.
FOCALITY: Unifocal.
TUMOR SIZE (LARGEST TUMOR IF MULTIPLE): 2.7 x 2.5 x 2.4 cm.
MACROSCOPIC EXTENT OF TUMOR: Tumor limited to kidney.
HISTOLOGIC TYPE: Clear cell (conventional) renal carcinoma.
HISTOLOGIC GRADE (FUHRMAN NUCLEAR GRADE): G1: Fuhrman
nuclear Grade I.
EXTENT OF INVASION
PRIMARY TUMOR (PT): pT1a: Tumor 4.0 cm or less in greatest
dimension, limited to kidney.
REGIONAL LYMPH NODES (PN): pNX: Cannot be assessed
DISTANT METASTASIS (PM): pMX: Cannot be assessed
MARGINS: Uninvolved by invasive carcinoma.
ADRENAL GLAND: Present.
VENOUS (LARGE VESSEL) INVASION (V): Absent.
LYMPHATIC (SMALL VESSEL) INVASION (L): Absent.
ADDITIONAL PATHOLOGIC FINDINGS: Mild nonspecific chronic
pyelitis.
Intradepartmental consultation obtained.
[T-71000 M-83103 189.0 P3-44070]