TCGA-WN-AB4C
Final Diagnosis:
A. Kidney, "right kidney mass", partial nephrectomy:
Papillary renal cell carcinoma (2.6 cm), Fuhrman nuclear grade 3. See comment.
Comment:
Kidney
Specimen Type: Partial nephrectomy
Laterality: Right
Tumor Site: Cannot be determined
Focality: Unifocal
Tumor Size (largest tumor if multiple): 2.6 x 2.0 x 2.0 cm
Histologic Type: Papillary renal cell carcinoma
Histologic (Fuhrman) Grade: G3: Nuclei very irregular, nucleoli large and prominent
Macroscopic Extent of Tumor: Confined to the kidney
Surgical Margins: Margin uninvolved by invasive carcinoma (focally the parenchymal resection is less than 0.5 mm from the tumor)
Adrenal Gland: Not applicable
Large vessel invasion ( renal vein, vena cava): Not applicable
Microvascular invasion: Not identified
Lymph Nodes:
Number Examined: None
Number Involved: Not applicable
Additional Pathologic Findings: None
Gross Picture Taken: No
Pathologic Staging: pT1a NX
