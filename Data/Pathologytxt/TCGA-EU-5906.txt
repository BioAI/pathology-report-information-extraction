TCGA-EU-5906
PROCEDURE: Radical nephrectomy.
SPECIMEN LATERALITY: Left.
TUMOR SITE: Upper pole.
TUMOR SIZE: Greatest dimension: 5.9 cm, Additional dimensions: 4.7 x 4.6 cm.
TUMOR FOCALlTY: Unifocal.
MACROSCOPIC EXTENT OF TUMOR: Tumor limited to kidney.
HISTOLOGIC TYPE: Clear cell renal cell carcinoma.
HISTOLOGIC GRADE (FUHRMAN NUCLEAR GRADE): G2.
LYMPH-VASCULAR INVASION: Not identified.
PATHOLOGIC STAGING:
Primary tumor: pT1b
Regional lymph nodes: pNX.
Distant metastasis: Not applicable.
