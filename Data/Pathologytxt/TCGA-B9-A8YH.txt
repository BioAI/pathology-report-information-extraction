TCGA-B9-A8YH
Diagnosis:
A: Kidney, right, robotic partial nephrectomy
Laterality: right
Histologic tumor type/subtype: papillary carcinoma, type I
Sarcomatoid features: absent
Histologic grade (if applicable): grade 2 (Fuhrman)
Tumor size (greatest dimension): 3.0 x 2.7 x 2.0 cm
Tumor focality: unifocal
Extent of tumor invasion (if present specify if macroscopic or
microscopic):
Capsular invasion/perirenal adipose tissue: not identified
Gerota' s fascia: not identified
Renal sinus: not applicable
Lymphatic (small vessel): not identified
Histologic assessment of surgical margins:
Renal parenchymal margin (partial nephrectomy only): negative
for malignancy
Renal capsular margin (partial nephrectomy only): negative for
malignancy
Paranephric adipose tissue margin (partial nephrectomy only):
negative for malignancy
AJCC Staging:
pT1a
pNX
