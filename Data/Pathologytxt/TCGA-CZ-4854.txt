TCGA-CZ-4854
PATHOLOGIC DIAGNOSIS:
REGIONAL LYMPH NODE, BIOPSY (including FSA):
~TwO lymph nodes, negative for tumor (0:2).
RIGHT KIDNEY, NEPHRECJOMY:
Report Status:
RENAL CELL CARCINOMA, ''clear cell type (5.7 cm), Fuhrman nuclear grade II.
Tumor is confined to the kidney.
No lymphovascular invasion is identified.
Vascular, ureteric, and soft tissue margins are negative for tumor.
Simple" cortical cyst.
AJCC Classification (6th Edition): T1b N0 MX

Renal pathology evaluation of non-neoplastic renal parenchyma diffuse mesangial expansion with focal nodular glomerulosclerosis
consistent \'lith early diabetic nephropathy (see note) .
focal global and segmental glomerulosclerosis, most likely secondary to
functional and structural adaptations (see note) .
severe arterial and hyaline arteriolar sclerosis (see note) .