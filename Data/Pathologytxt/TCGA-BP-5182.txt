TCGA-BP-5182

DIAGNOSIS:
1. SP: Kidney, left, partial nephrectomy
Tumor Type:
Renal cell carcinoma- Conventional (clear cell) type
Fuhrman Nuclear Grade:
Nuclear grade III/IV
Tumor Size:
Greatest diameter is 3.0 cm.
Local Invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not identified
Small vessel angiolymphatic invasion is also not identified.
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Mild arteriosclerotic changes
Adrenal Gland:
Not identified
Lymph Nodes:
Not Identified
Staging for renal cell carcinoma/oncocytoma:
pT1 Tumor<= 7.0 cm in greatest dimension limited to the kidney
