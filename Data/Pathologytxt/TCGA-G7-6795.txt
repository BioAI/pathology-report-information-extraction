TCGA-G7-6795
Specimens: A: LEFT KIDNEY 
Specimen Type: Radical nephrectomy 
With adrenal gland 
Laterality: Left 
Tumor Site: Upper pole 
Focality: Unifocal 
Tumor Size (largest tumor if multiple): Greatest dimension: 2.5cm 
Additional dimensions: 2.5cm x 1.8cm 
Macroscopic Extent of Tumor: Tumor limited to kidney 
WHO CLASSIFICATION 
Papillary renal cell carcinoma 8260/3 
Histologic Grade (Fuhrman Nuclear Grade): G2: Nuclei slightly irregular, approximately 15 u; nucleoli evident 
Invasion of Vascular/Lymphatic: Absent 
Perinephric Tissue Invasion: Absent 
Margins: Margins uninvolved by invasive carcinoma 
Adrenal Gland: Uninvolved by tumor 
Regional Lymph Nodes: Negative 0 / 2 
Additional Findings: Interstitial disease 
type::chronic interstitial pyelonephritis cystic changes
Pathological Staging (pTNM): pT1a N0 Mx
