TCGA-4A-A93W
1.Right kidney (14 grams ), partial nephrectomy: Papillary renal cell carcinoma.
2.Right kidney (deep margin): Papillary renal cell carcinoma ; new margin free of tumor.
SYNOPTIC REPORT
Tumor site:Right kidney
Histologic type: Papillary renal cell carcinoma
- Sarcomatoid features:Not identified
Histologic (Fuhrman) grade:G2
Tumor size:3.6 cm
Tumor focality:Unifocal
Extent of tumor:Tumor is limited to kidney
Parenchymal margin:Negative for malignancy (1.0 cm from tumor)
Angiolymphatic Invasion:Not identified
Surgical Pathology Staging:pTla NX
