TCGA-BP-5181
KIDNEY; PARTIAL
Tumor Type: 
Renal cell carcinoma- Conventional (clear cell) type 
Fuhrman Nuclear Grade: Nuclear grade II/IV 
Tumor Size: Greatest diameter Is 4.2 cm
Local Invasion (for renal cortical types): Not Identified 
Renal Vein Invasion: Not identified 
Surgical Margins: Free of tumor
Non-Neoplastic Kidney: Unremarkable 
Adrenal Gland: Not identified 
Lymph Nodes: Not identified 
Staging for renal cell carcinoma/oncocytoma: pT1 Tumor<= 7.0 cm in greatest dimension limited to the kidney