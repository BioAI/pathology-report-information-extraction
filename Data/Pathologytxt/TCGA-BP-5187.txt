TCGA-BP-5187
DIAGNOSIS:
1. Kidney, right upper pole, partial nephrectomy:
Tumor Type:
Renal cell carcinoma -Conventional (clear cell) type
Fuhrman Nuclear Grade:
Nuclear grade II/IV
Tumor Size:
Greatest diameter is 3.5 cm.
Local Invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not identified
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Focal chronic inflammation and vascular sclerosis
Adrenal Gland:
Not identified
Lymph Nodes:
Not Identified
Staging for renal cell carcinoma/oncocytoma:
pT1 Tumor<= 7.0 cm in greatest dimension limited to the kidney