TCGA-DV-5565(3)
DIAGNOSIS:
3. "Tumor #5", right kidney (partial nephrectomy): Renal cell carcinoma, clear cell type, Furman nuclear grade 2.
The specimen consists of yellow soft tissue mass measuring 3.1 x 2.6 x 1.2 cm.