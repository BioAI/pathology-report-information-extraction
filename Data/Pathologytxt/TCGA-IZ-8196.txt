TCGA-IZ-8196
FINAL DIAGNOSIS:
A. RIGHT PER.I RENAL FAT_
ADIPCSE TISSUE. NO CARCINOMA SEEN.
B. RICHT RENAL MASS-
RENAL CELL CARCTNOMA, LIMITED TO RENAL TTSSUE EXAMINED
PROPORTION OF SAF.COMATOID COMPONENT: 0%
SPECIMEN LATERALITY: RTGHT
TUMOR FOCALITY: UNIFOCAL.
TUMOR SIZE (LARGBST TUMOR, IF MULTIPLE) : 4 CM.
HISTOLOGIC TYPE: PAPTLLARY, TYPE 1.
IIISTOLOGIC GRADE (FUHRMAN NUCLEAR GRADE) : 2/ 4.
TNM STAGE: pTa, pNX, pMX.
