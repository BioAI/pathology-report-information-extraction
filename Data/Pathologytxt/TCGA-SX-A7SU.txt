TCGA-SX-A7SU
DIAGNOSIS:
A. Periaortic and parahilar lymph nodes: Eight lymph nodes,
negative for malignancy (0/8).
B. Mass left kidney, resection: Papillary renal cell carcinoma,
Fuhrman nuclear grade 2 (of 4), multifocal, the largest tumor
measuring 2.0 cm in greatest dimension. The carcinoma focally bulges
perinephric adipose tissue, but no invasion of perinephric adipose
tissue is identified. The renal parenchymal margin of resection is
negative for malignancy. No lymphatic-vascular invasion is
identified (see Comment).
