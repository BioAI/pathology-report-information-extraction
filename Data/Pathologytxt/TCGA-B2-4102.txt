TCGA-B2-4102
DIAGNOSIS
Kidney, right, resection:
Clear cell type renal cell carcinoma, Furhman grade 2.
Tumor type: Clear type renal cell carcinoma.
Tumor grade: 2 (out of 4; Fuhrman et al. AJSP 6:655,)
Tumor size: 5 cm.
Renal capsule: Negative for malignancy.
Renal sinus: Negative for malignancy.
Vascular invasion: Absent.
Hilar margins: Negative for malignancy.
Kidney away from tumor: Unremarkable.
pTNM Stage: T1b
