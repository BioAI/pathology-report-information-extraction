TCGA-A3-3359
Final Diagnosis
Left kidney (nephrectomy):
Tumor:
Histologic type: Renal cell carcinoma (clear cell type).
Tumor site: Upper pole.
Tumor size: 4.0 cm in greatest diameter.
Macroscopic extent of tumor: Tumor limited to kidney.
Nuclear grade: Fuhrman grade 2/3
Lymphovascular space invasion: No unequivocal lymphovascular space invasion identified.
Transcapsular invasion: No
Renal vein invasion: No
Vena caval invasion: Not resected
Adrenal gland: Not resected.
Surgical margin status:
Soft tissue margins: Not involved
Ureteral surgical margins.: Not involved
Vascular surgical margins: Not involved
Lymph node 'status: No lymph nodes resected.