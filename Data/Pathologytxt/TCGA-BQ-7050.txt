TCGA-BQ-7050

1.Kidney,left,upper pole  tumor biopsy
Tumor Type:Renal cell carcinoma-Papillary type
Fuhrman Nuclear Grade: Nuclear grade III/IV
Tumor Size: Greatest diameter is 2.2 cm.
Renal Vein Invasion: Not identified
Lymph Nodes:Not identified
Staging for renal cell carcinoma/oncocytoma:
pT1 Tumor <= 7.0 cm in greatest dimension limited to the kidney