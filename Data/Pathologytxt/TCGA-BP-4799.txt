TCGA-BP-4799
1. SP: Right kidney and adrenal gland, radical nephrectomy:
Tumor Type: Renal call carcinoma- Conventional (clear cell) type
Fuhrman Nuclear Grade: Nuclear grade III/IV
Tumor Size: Graatast diameter is 6.0 cm.
Local Invasion (for renal cortical types): Extends through renal capsule but confined within Garota's fascia, Involves renal sinus fat
Renal Vein Invasion: Not Identified. However, muscular branches of renal vain in the hilum are Invaded by the tumor
Surgical Margins: Free of tumor
Non-Neoplastic Kidney: Mild arteriosclerotic changes, benign cortical cysts, perirenalorganizing hemorrhage
Adrenal Gland: Not involved .Multiple foci of cortical amyloidosis (congo rod positive); vessels and soft tissues not Involved by amyloidosis.
Stains to characterize the amyloid deposists are being performed, and results on these will be reported as an addendum.
Lymph Nodes: Not ldentifled
Staging for renal cell carcinoma/oncocytoma: pT3b Tumor grossly extends into the renal vain(s) or vena cava below the diaphragm
