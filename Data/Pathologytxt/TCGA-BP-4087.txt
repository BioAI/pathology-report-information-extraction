TCGA-BP-4087

DIAGNOSIS:
1. Kidney, right; partial nephrectomy:
Tumor Type:
Renal cell carcinoma -Conventional (clear cell) type
Fuhrman Nuclear Grade:
Nuclear grade III/IV
Tumor Size:
Greatest diameter is 1.9 cm.
Local Invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not applicable
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Unremarkable
Adrenal Gland:
Not Identified
Lymph Nodes:
Not identified
Staging for renal cell carcinoma/oncocytoma:
pT1 Tumor<= 7.0 em In greatest dimension limited to the kidney
