TCGA-BP-5185
DIAGNOSIS:
1. SP: Kidney, right, partial nephrectomy:
Tumor Type: Renal cell carcinoma - Conventional (clear cell) type
Fuhrman Nuclear Grade:
Nuclear grade 3/4
Tumor Size:
Greatest diameter is 3.3 cm

Local invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not identified
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Unremarkable
Adrenal Gland:
Not Identified
Lymph Nodes:
Not Identified
Staging for renal cell carcinoma/oncocytoma:
pT1 Tumor<= 7.0 cm in greatest dimension limited to the kidney