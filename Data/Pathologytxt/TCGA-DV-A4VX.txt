TCGA-DV-A4VX
DIAGNOSIS:
1. Kidney and adrenal gland, right (radical nephrectomy and adrenectomy ): Renal cell carcinoma , clear cell type,
Fuhrman nuclear grade IV; tumor involves the renal vein; prominent vascular invasion is present; tumor
penetrates through the capsule with involvement of perinephric tissue; tumor present in the lymphatic spaces
surrounding the ureteral margin; arterial margin free of tumor; hilar lymph nodes free of tumor (0/2); adrenal gland
free of tumor, inked margin of resection free of tumor.
2. Lymph node, intraaortic cava (lesion): An atypical cell is present in one of four lymph nodes.
3. Lymph node, perlcaval (excision): Reactive lymph nodes, no tumor seen (0/19).
NOTE:
Kidney Nephrectomy Summary Table macroscopic
Specimen Type : Radical nephrectomy and adrenectomy
Laterality : Right
Tumor Size: 11.5 x7.4 cm
Focality : Unifocal
Macro Extent of Tumor: Tumor extends into perinephric tissue
Microscopic
Histologic Type : Renal cell carcinoma, clear cell type.
Histologic Grade: Fuhrman nuclear grade IV
Extent of Invasion
Primary Tumor(pT): pT3b
Regional Lymph Nodes(pN): An atypical cell present in one of the intraaortic lymph nodes.
Distant Metastlsis(pM): pMx
Margins: Free of tumor
CLINICAL INFORMATION: Allocate Order to Protocol: Brief Clinical History: huge
right renal mass with lung metastases and retroperitoneal
lymphadenopathy Specimen Taken For Protocol: 01 - Yes
PROCEDURE: Pre-Operative Diagnosis: right renal mass Post-Operative Diagnosis:
same Operative Findings: large right renal mass with vena cava tumor
thrombus, bulky retropertitoneal lymphadenopathy
SPECIMENS SUBMITTED: 1. KIDNEY, RIGHT
2. LYMPH NODES, Interaortocaval
3. LYMPH NODES, Paracaval
