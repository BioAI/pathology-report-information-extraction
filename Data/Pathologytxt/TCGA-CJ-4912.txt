TCGA-CJ-4912
DIAGNOSIS
(A) LEFT KIDNEY AND ADRENAL GLAND:
RENAL CELL CARCINOMA, CONVENTIONAL (CLEAR CELL) TYPE,
FUHRMAN'S NUCLEAR GRADE 3. (15.0 CM)
No invasion of renal sinus adipose tissue or renal vein.
Vascular, ureteral, and soft tissue margins of resection free of tumor.
Adrenal gland, free of tumor. 