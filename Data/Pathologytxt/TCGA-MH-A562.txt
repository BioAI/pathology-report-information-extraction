TCGA-MH-A562

DIAGNOSIS:
Kidney, left, partial nephrectomy:
- PAPILLARY RENAL CELL CARCINOMA
PROCEDURE: Partial nephrectomy
SPECIMEN LATERALITY: Left
TUMOR SIZE:
Greatest dimension: 1.8 cm
Additional dimensions: 1.8 x 0.8 cm
TUMOR FOCALITY: Unifocal
MACROSCOPIC EXTENT OF TUMOR: Tumor limited to kidney
HISTOLOGICAL TYPE: Papillary renal cell carcinoma
SARCOMATOID FEATURES: Not identified
TUMOR NECROSIS: Not identified
HISTOLOGICAL GRADE: Fuhrman Nuclear Grade 3
NOTE: The tumor is predominantly Type 1; however, there are areas of
Type 2 histology which show nuclear grade 3 features.
MICROSCOPIC TUMOR EXTENSION: Tumor limited to kidney
MARGINS: Margins uninvolved by invasive carcinoma
LYMPH-VASCULAR INVASION: Not identified.
PATHOLOGICAL STAGING:
Primary Tumor: pTla
Regional Lymph nodes: pNX
Distant Metastasis: Not applicable
