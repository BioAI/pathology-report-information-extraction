TCGA-A3-3378
Left kidney, nephrectomy:
Renal carcinoma.
Tumor Characteristics:
1. Histologic type: Clear cell adenocarcinoma.
2. Tumor site: Posterior aspect of mid left kidney.
3. Tumor size: 5.5 x 5.0 x 4.0 cm.
4. Macroscopic extent of tumor: Limited to kidney.
5. Nuclear grade: Fuhnnan grade: 3/4.
6. Lymphovascular space invasion: No.
7. Transcapsular invasion: No.
8. Renal vein invasion: No.
9. Venacaval invasion: Not resected.
10. Adrenal gland: Not identified.
Surgical Margin Status:
1. Soft tissue margins: Negative for tumor.
2. Ureteral margin: Negative for tumor.
3. Vascular Margins: Negative for tumor.
Lymph Node Status:
1. No lymph nodes identified.
Other Significant Findings:
pTNM stage: T1N0MX, Stage I.