TCGA-A3-3385
Histologic type: Renal cell carcinoma, clear cell type
Site: Neoplasm is identified on the posterior lateral aspect in the mid region of the kidney.
Size: 3.5 x 3.0 x 3.0 cm
Nuclear grade: Fuhrman grade 2/4
Lymphovascular space invasion: Extensive perivasular neoplasm is identified but no intraluminal tumor is seen.
Renal vein invasion: No
Stage: pT1a, Nx, Mx
