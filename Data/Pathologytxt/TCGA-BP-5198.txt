TCGA-BP-5198
DIAGNOSIS:
1. Kldney, right upper pole; partlal nephrectomy:
Tumor Type:
Renal cell carcinoma : Conveolional (clear cell)type
Fuhrman Nuclear Grade:
Nuclear grade III/IV
Tumor Size:
Greatesl diameter is 6.0 cm
Local Invaslon (for renal cortical types):
Involves renal sinus fat
Renal Veln Invasion:
Tumor invades multiple muscular branches of renal vein in the renal sinus
Surgical Margins:
Tumor present at renal parenchymal margin (for partia) nephrectomy specimens only)
Non-Neoplastic Kidney:
Cortlcal scars, benign cortical cysts, and mild arteriosclerotic changes
Adrenal Gland;
Not identified
Lymph Nodes:
Not identified
Staging for renal cell carcinoma/oncocyloma:
pT3b Tumor grossly exlends into the renal vein(s) or vena cava below the diaphragm
2. Lymph nodes, pericaval; biopsy:
Lymph Nodes:
Not involved
Number of nodes examlned:9
