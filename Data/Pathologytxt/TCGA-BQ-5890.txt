TCGA-BQ-5890
SP: Left kidney and ureter
Tumor type: renel cell carcinoma - unclassified type (combination with papillary clear cell)
tumor size: greatest diameter is 11.5cm
renel vein invasion: not identified
surgical margins: free of tumor
adrenal gland: not identified
lymph nodes: not identified
staging for renel cell carcinoma: pT31 Tumor invades the adrenal gland