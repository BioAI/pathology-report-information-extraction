TCGA-B8-5162
Diagnosis:
Kidney, left., partial nephrectomy
Histologic tumor type/subtype: renal cell carcinoma, clear cell type
Histologic grade (if applicable): Fuhrm.an nuclear grade 2 (of 4)
Tumor size (greatest dimension): 7.3 cm
Extent of tumor invasion:
Capsular invasion/perirenal adipose tissue: negative for tumor
Gerota's fascia: negative for tumor
Renal vein: N/A
Ureter: segment of ureter, negative for tumor
Venous (large vessel): negative for tumor
Lymphatic (small vessel): negative for tumor
Histologic assessment of surgical margins:
Perirenal adipose tissue: negative for tumor
Gerota's fascia: negative for tumor
Renal vein: N/A
Renal artery: N/A
Ureter: negative for tumor
Adrenal gland: not present
Lymph nodes: none identified
Other significant findings:
- Segment of ureter with no significant pathologic abnormality, negative for malignancy
- Non-neoplastic kidney with mild to moderate arterionephroscterosis
AJCC Staging:
pT2a pNX
