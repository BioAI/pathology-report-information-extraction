TCGA-CJ-6032
DIAGNOSIS
(A) RIGHT KIDNEY:
RENAL CELL CARCINOMA (7.0 CM MAXIMUM DIMENSION), CONVENTIONAL TYPE
(95% CLEAR CELLS, 5% EOSINOPHILIC CELLS), FUHRMAN NUCLEAR GRADE 3, CONFINED TO
THE KIDNEY.
(SEE COMMENT)
Margins of resection free of tumor.
