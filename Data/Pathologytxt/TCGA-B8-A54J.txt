TCGA-B8-A54J
Diagnosis:
A: Kidney, right, nephrectomy
Tumor histologic type: renal cell carcinoma, clear cell subtype
Sarcomatoid features: not identified
Histologic grade: 2 (of 4, Fuhrman classification)
Tumor size: 8.6 cm
Tumor focality: unifocal
Extent of invasion:
Extra-capsular invasion into perirenal adipose tissue: not identified
Gerota' s fascia: present, not involved
 Renal sinus: not involved
Major veins (renal vein or segmental branches, IVC): not involved
Ureter: not involved
Venous: not identified
Lymphatic: not identified
Surgical margins:
Perinephric adipose tissue margin: not involved
Renal vein: not involved
Ureter: not involved
Adrenal gland: not present for evaluation
Lymph nodes: none present for evaluation
Pathologic findings in non-neoplastic kidney:
arteriolonephrosclerosis,
focal simple cyst, focal adenoma, variable interstitial chronic inflammation
AJCC Stage: pT2a pNX pMX
This staging information is based on information available at the time of this report, and is subject to change pending clinical review and
additional information.