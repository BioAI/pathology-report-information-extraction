TCGA-AK-3445
Final Diagnosis:
1. Left kidney, radical nephrectomy:
- Clear cell renal cell carcinoma (5.5 cm). Fuhnnan nuclear grade III.
Tumor invades into perinephrc fat. See note.
- Vascular invasion is not identified.
- Renal vein, ureteral and perinephric fat (circumferential) margins are free of tumor.
- Benign adrenal gland. 
Note: Tumor cells are positive for CD10, Pancytokeralin and Vimentin and are negative for CD117 supporting the diagnosis.
pT3 NX MX.