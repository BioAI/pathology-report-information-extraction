TCGA-BP-5000
DIAGNOSIS:
1. KIDNEY, LEFT, PARTIAL NEPHRECTOMY 
Tumor Type:
Renal cell carcinoma- Conventional (clear cell) type
Fuhrman Nuclear Grade:
Nuclear grade III/IV
Tumor Size:
Greatest diameter is 5.4 cm.
Local invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not identified
Small vessel anglolymphatlc Invasion also not identified
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Compression-related changes
Adrenal Gland:
Not identified
Lymph Nodes:
Not identified
Staging for renal cell carcinqma/oncocytoma:
pT1 Tumor <=7.0 cm In greatest dimension limited to tho kidney