TCGA-CJ-4907
DIAGNOSIS
(A) RIGHT KIDNEY, ADRENAL, AND IVC THROMBUS:
RENAL CELL CARCINOMA (6.0 CM MAXIMUM DIMENSION), CONVENTIONAL TYPE (90% CLEAR CELLS, 10%
EOSINOPHILIC CELLS), FUHRMAN NUCLEAR GRADE 3, INVASIVE INTO RENAL VEIN AND RENAL
SINUS ADIPOSE TISSUE. (SEE COMMENT)
Renomedullary Interstitial cell tumor (0.4 cm).
Margins of resection free of tumor.
Adrenal gland, no tumor present.
COMMENT
The renal cell carcinoma extensively Involves the renal sinus adipose tissue, including vascular-lymphatic invasion. The perinephric
adipose tissue is free of tumor. The tumor invades into the renal vein and inferior vena cava. The tumor thrombus protrudes beyond the
renal vein margin; however, the wall of the renal vein at the margin is free of tumor.
