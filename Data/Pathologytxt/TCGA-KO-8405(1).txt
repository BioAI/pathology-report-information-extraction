TCGA-KO-8405
PATHOLOGIC DIAGNOSIS: 
A/1) RIGHT KIDNEY, RADICAL NEPHRECTOMY: 
RENAL CELL CARCINOMA, chromophobe type (9. 7 cm), Fuhrman nuclear grade 2 (of 4), arising in the upper pole.
No lymphovascular invasion. 
Tumor is limited to kidney. 
Renal artery, vein, ureteral m DIAGNOSIS
(A) PORTION OF 11TH RIB:
Segment of rib for gross inspection only.
(B) LEFT PARTIAL KIDNEY MID ASPECT:
CHROMOPHOBE RENAL CELL CARCINOMA, EOSINOPHILIC TYPE, FUHRMAN'S NUCLEAR GRADE 3.
(SEE COMMENT)
TUMOR FOCALLY EXTENDS INTO PERINEPHRIC ADIPOSE TISSUE.
TUMOR MEASURES 4.5 CM IN MAXIMUM DIMENSION.
Parenchymal and soft tissue margins of resection free of tumor.
(C) LEFT PARTIAL KIDNEY MID ASPECT, INFERIOR MARGIN:
Renal parenchyma, no tumor present.
(D) LEFT PARTIAL KIDNEY, MID ASPECT, INFERIOR/LATERAL:
Renal parenchyma, no tumor present.
(E) LEFT PARTIAL KIDNEY MID ASPECT, LATERAL MARGIN:
Renal parenchyma, no tumor present.
(F) LEFT PARTIAL KIDNEY, MID ASPECT, SUPERIOR MARGIN:
Renal parenchyma, no tumor present.
(G) LEFT PARTIAL KIDNEY, MID ASPECT, SUPERIOR/LATERAL:
Renal parenchyma, no tumor present.
(H) LEFT PARTIAL KIDNEY, MID ASPECT, DEEP/MEDIAL:
Renal parenchyma, no tumor present.
(I) LEFT PARTIAL KIDNEY, DEEP/LATERAL:
Renal parenchyma, no tumor present.
(J) LEFT ADRENAL GLAND:
Adrenal gland, no tumor present.
(K) LEFT PERIAORTIC LYMPH NODES:
Seven lymph nodes, no tumor present

