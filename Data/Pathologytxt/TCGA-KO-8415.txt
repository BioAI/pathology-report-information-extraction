TCGA-KO-8415
DIAGNOSIS
(A) UTERUS AND CERVIX:
Proliferative endometrium with adenomyosis.
Myometrial leiomyomata.
Chronic cervicitis.
(B) RIGHT KIDNEY:
RENAL CELL CARCINOMA, CHROMOPHOBE TYPE, FUHRMAN NUCLEAR GRADE 3,
(7.0 CM IN MAXIMUM DIMENSION).
TUMOR IS LIMITED TO KIDNEY.
Margins of resection are free of tumor.
No renal vein invasion is present.