TCGA-UW-A72N
Diagnosis:
A. Kidney, left, partial nephrectomy
Histologic tumor type/subtype: chromophobe renal cell carcinoma
Sarcomatoid features: not identified
Histologic grade (if applicable): not applicable
Tumor size (greatest dimension): 3.8 cm by gross examination
Tumor focality: unifocal
Extent of tumor invasion (if present specify if macroscopic or microscopic):
Capsular invasion/perirenal adipose tissue: not identified
Gerota' s fascia: not applicable
Renal sinus: not identified
Major veins (renal vein or segmental branches, IVC): not identified
Ureter: not applicable
Venous (large vessel): not identified
Lymphatic (small vessel): not identified
Histologic assessment of surgical margins:
Renal parenchymal margin (partial nephrectomy only): negative
Renal capsular margin (partial nephrectomy only): negative
Perinephric adipose tissue margin (partial nephrectomy only): negative
Adrenal gland: not submitted
Lymph nodes: none identified
Other significant findings: none
AJCC Staging: pT1a pNx