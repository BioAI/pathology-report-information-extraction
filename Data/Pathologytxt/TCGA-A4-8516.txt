TCGA-A4-8516
Diagnosis
right kidney mass partial nephrectomy
1. renel cell carcinoma, papillary type 1, 3.6cm
2. tumor focally extends into perinephric adipose tissue
3. surigcal margins are negative for tumor
4. negative for lymphatic/vascular invasion by tumor
5. renel cell carcinoma clear cell tyoe Fuhrman grade 2 of 4, 0.1cm incidental finding confined to the kidney
6. residual renal parenchyma with papillary adenomas