TCGA-NP-A5GY
RENAL MASS, LEFT LOWER POLE, PARTIAL NEPHRECTOMY:
CHROMOPHOBE RENAL CELL CARCINOMA, NUCLEAR GRADE 2
- EOSINOPHILIC TYPE, 2.2 CM.
RENAL PARENCHYMAL MARGIN FOCALLY POSITIVE.
PATHOLOGIC TUMOR STAGING SUMMARY:
-Histologic type and grade: Chromophobe renal cell carcinoma, eosinophilic type,nuclear grade 2 out of 4.
-Primary tumor: pTIa,2.2 cm.
- Regional lymph nodes: NA.
- Distant metastasis: NA
- Pathologic stage: I.
- Margin status: RI, focally positive at renal parenchymal (resection) margin.
-Lymphovascular and perineural invasion: Not identified