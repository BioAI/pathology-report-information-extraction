TCGA-P4-A5E6
DIAGNOSIS
(A) LEFT 12TH RIB:
Segment of rib, for gross inspection only.
(B) LEFT KIDNEY AND ADRENAL:
PAPILLARY RENAL CELL CARCINOMA, FUHRMAN'S NUCLEAR GRADE 3.
TUMOR CONFINED TO THE KIDNEY.
TUMOR MEASURES 6.5 CM IN MAXIMUM DIMENSION.
Vascular ureteral and soft tissue margins of resection free of
tumor.
Multiple renal cortical adenomas (0.3 cm in maximum dimension).
Multiple renal cortical ( simple) cysts (5.0 cm in maximum dimension).
Adrenal cortical hyperplasia.
(C) HILAR LYMPH NODES:
METASTATIC PAPILLARY RENAL CELL CARCINOMA IN ONE OF THREE LYMPH
NODES. (SEE COMMENT)
