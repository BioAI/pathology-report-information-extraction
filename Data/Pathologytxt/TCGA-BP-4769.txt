TCGA-BP-4769
DIAGNOSIS:
1. KIDNEY, LEFT, PARTIAL NEPHRECTOMY:
Tumor Type:
Renal cell carcinoma- Conventional (clear cell) type
A second renal cell carcinoma, papillary type, Is present
Fuhrman Nuclear Grade:
The clear cell carcinoma is nuclear grade II/IV. The papillary tumor is type I
Tumor Size:
Greatest diameter is 2.2 cm.
The size of the papillary tumor Is 1.1 cm
Local Invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not identified
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
2 minute so called "papillary tubular adenomas" are present
Adrenal Gland:
Not identified
Lymph Nodes:
Not Identified
Staging for renal cell carcinoma/oncocytoma:
pT1 Tumor <= 7.0 cm in greatest dimension limited to the kidney
