TCGA-UW-A72H
Diagnosis:
Kidney, right, nephrectomy
Histologic tumor type/subtype: Chromophobe renal cell carcinoma
Sarcomatoid features: None identified
Histologic grade (if applicable): Fuhrman grade 4
Tumor size (greatest dimension): 5.2 cm
Tumor focality: Unifocal
Extent of tumor invasion (if present specify if macroscopic or
microscopic):
Capsular invasion/perirenal adipose tissue: Negative
Gerota' s fascia: Negative
Renal sinus: Positive; microscopic (slide A9)
Major veins (renal vein or segmental branches, IVC): Negative
Ureter: Negative
Venous (large vessel): Negative
Lymphatic (small vessel): Negative
Histologic assessment of surgical margins:
Gerota' s fascia (nephrectomy): Negative
Renal vein (nephrectomy): Negative
Ureter (nephrectomy): Negative
Adrenal gland: Negative for carcinoma
Lymph nodes: Not applicable
AJCC Staging: pT3a pNX