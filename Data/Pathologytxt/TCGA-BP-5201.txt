TCGA-BP-5201
DIAGNOSIS:
1. Kidney, left, total nephrectomy
Tumor Type: Renal cell carcinoma -Conventional (clear cell) type 
Fuhrman Nuclear Grade: Nuclear gratie IV/IV 
Tumor Size: Greatest diameter is 9.5 cm.
Local Invasion (for renal cortical types): Extends through renal capsule but confined within Gerota's fascia 
Renal Vein Invasion: Tumor is identified in muscular vessels in the region of the renal sinus/hilum 
Surgical Margins: Free of tumor
Non-Neoplastic Kidney: Unremarkable 
Adrenal Gland: Involved by tumor (see Part 2) 
Lymph Nodes: Not Identified 
Staging for renal cell carcinoma/oncocytoma:pT3b Tumor grossly extends into the renal vein(s) or vena cava below the diaphragm