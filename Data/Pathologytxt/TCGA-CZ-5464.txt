TCGA-CZ-5464
SPECIMEN DESIGNATED "LEFT KIDNEY NITH PIEC OF RENAL VEIN":
RENAL CELL CARCINOMA, clear cell typ , Fuhrman nuclear grade II,
multifocal, in kidney parenchyma (largest focus, 7.5 cm, in upper
pole to middle region), extension into perirenal soft tissue and renal vein
(see NOTE) .
Renal artery, inked margins, and Gerotals fascia are negative for tumor.
Adrenal gland is negative for tumor.
Renal vein margin is positive for tumor.
Lymphovascular invasion is present in perirenal blood vessels.
AJCC Classification (6th Edition): T3b NX MX.
NOTE:
Some of the separate nodules grossly appear to be due to venous spread.
CLINICAL DATA:
History: None given.
Operation: Left radical nephrectomy with
Operative Findings: None given.
Clinical Diagnosis: Left renal mass.