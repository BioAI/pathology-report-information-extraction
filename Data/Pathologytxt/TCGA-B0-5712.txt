TCGA-B0-5712
PROCEDURE: Left radical nephrectomy.
FINAL DIAGNOSIS:
A. RENAL CELL CARCINOMA. CONVENTIONAL (CLEAR) CELL TYPE (17.8 CM).
8. FUHRMAN NUCLEAR GRADE IS 3 OF 4.
C. THE NEOPLASM IS CONFINED WITHIN THE RENAL CAPSULE.
D. NO INVASION OF THE RENAL VEIN IS IDENTIFIED.
E. NO EVIDENCE OF ANGIOLYMPHATIC INVASION IS IDENTIFIED.
F. ALL SURGICAL MARGINS ARe FReE OF THE NEOPLASM.
G. THE NON-NEOPLASTIC KIDNEY DEMONSTRATES CHANGES OF YDRONEPHROSIS.
H. THE ADRENAL GLAND IS NOT SUBMITTED.
I. HISTOPATHOLOGIC STAGE: pT2 NX MX
J. TNM HISTOLOGIC GRADE = G3.