TCGA-BP-4982
DIAGNOSIS:
1. SP: Kidney, left, partial nephrectomy :
Tumor Type:
Renal cell carcinama Conventional (clear cell) type
Fuhnnan Nuclear Grade:
Nuclear grade III/IV
Tumor Size:
Greatest diameter Is 4.5 cm.
Local Invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not identified
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Interstitial fibrosis and chronic inflammation, tubular atrophy
Adrenal Gland:
Not identified
Lymph Nodes:
Not identified
Staging for renal cell carcinama :
pT1 Tumor<= 7.0 cm In greatest dimension limited to the kidney
