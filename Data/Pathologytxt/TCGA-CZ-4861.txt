TCGA-CZ-4861
PATHOLOGIC DIAGNOSIS :
LEFT RADICAL NEPHRECTOMY ;
CLEAR CELI, RENAL CELL CARCINOMA (8.2 cm)��Fuhrman nuclear grade II/IV, arising in the lower pole with foci of tumor necrosis and hyalinization.
Approximately 60% of the tumor mass is viable.
Arterial, venous, and ureteral margins are negative for tumor.
Tumor is limited to the kidney and comes to within 0.1 cm of the Gerota's fascia margin.
No lymphovascular invasion is seen.
No venous invasion is seen.
Simple cortical cyst (1.3 cm) ��
The non- neoplastic kidney will be evaluated by Rena1 Pathology, and the findings will be reported in an addendum. .
AJCC Classification: T2 NX MX.