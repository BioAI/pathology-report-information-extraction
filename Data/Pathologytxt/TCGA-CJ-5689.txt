TCGA-CJ-5689

LEFT KIDNEY:
RENAL CELL CARCINOMA, CONVENTIONAL (60% CLEAR CELLS AND 40% EOSINOPHILIC CELLS), FUHRMAN'S
NUCLEAR GRADE 4. (SEE COMMENT)
TUMOR MEASURES 6.5 CM IN MAXIMUM DIMENSION.
Vascular, urethral and soft tissue margins of resection are free of tumor.
Renal cortical (simple) cysts, multiple (ranging from 0.1 to 6.0 cm in maximum dimension).
