TCGA-P4-AAVM
LEFT KIDNEY AND ADRENAL AND PARA-AORTIC LYMPH NODE:
PAPILLARY RENAL CELL CARCINOMA (2.3 X 2.0 X 1.5 CM), TYPE 2, FUHRMAN NUCLEAR GRADE 3,
CONFINED TO THE KIDNEY.
Multiple renal adenomas.
Renal cortical cysts.
Renal parenchyma with multiple foci of calcifications and focal fibrosis.
Margins of resection (vascular, ureteral and perinephric adipose tissue ) free of tumor.
Adrenal cortical adenoma (4.9 x 4.5 x 3.1 cm, 43.8 grams).