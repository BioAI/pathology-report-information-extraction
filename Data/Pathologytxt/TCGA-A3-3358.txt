TCGA-A3-3358
Final Diagnosis
Left kidney:
Tumor characteristics:
1. Histologic type: Renal cell carcinoma, clear cell type.
2. Tumor site: Lower pole.
3. Size: 2.7 cm in greatest diameter.
4. Macroscopic extent of tumor: Tumor limited to kidney.
5. Nuclear grade: 1-2/4
6. Lymphovascular space invasion: Focal tumor identified within endothelial-lined lymphovascular space.
7. Transcapsular invasion: No.
8. Renal vein invasion: No.
9. Vena caval invasion: Not resected.
Surgical margin status:
1. Soft tissue margins: Negative.
2. Ureteral margin: Negative.
3. Vascular margin: Negative.
Lymph node status:
No lymph nodes resected.
Other:
Left adrenal gland: No pathologic diagnosis.
CPT: 88307 x 2
Comments
This test has been finalized at the ••••••• campus.
