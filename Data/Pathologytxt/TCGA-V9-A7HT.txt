TCGA-V9-A7HT
A: KIDNEY, NEPHRECTOMY
SPECIMEN TYPE: Radical nephrectomy
LATERALITY: Right 
*TUMOR SITE: *Upper pole
FOCALITY: Unifocal
TUMOR SIZE (largest tumor if multiple):
Greatest dimension: 8.0 cm
*Additional dimensions: 7.1 x 5.3 cm
MACROSCOPIC EXTENT OF TUMOR: Tumor limited to kidney
HISTOLOGIC TYPE: Papillary renal carcinoma
HISTOLOGIC GRADE (Fuhrman Nuclear Grade):
G3: Nuclei very irregular, -20 microns; nucleoli large and promienent
PRIMARY TUMOR (pT)PRIMARY TUMOR (pT):
pT2: tumor more than 7 cm in greatest dimension, limited to the kidney
REGIONAL LYMPH NODES:
pNX: cannot be assessed
DISTANT METASTASIS (pM):
pMX: Cannot be assessed
MARGINS: Margins uninvolved by invasive carcinoma
ADRENAL GLAND: Not present
*VENOUS (LARGE VESSEL) INVASION (V) (excluding renal vein and inferior vena cava): *Absent
*LYMPHATIC INVASION (L): *Absent
*ADDITIONAL PATHOLOGIC FINDINGS: *Inflammation (type): chronic
*COMMENT(S): - The tumor exhibits histologic features of type-2 papillary renal cell carcinoma.