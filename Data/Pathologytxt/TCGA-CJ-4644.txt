TCGA-CJ-4644
(A) LEFT KIDNEY AND ADRENAL:
RENAL CELL CARCINOMA (10.0 CM MAXIMUM DIMENSION), 
CONVENTIONAL TYPE (70% CLEAR CELLS, 30% EOSINOPHILIC CELLS), 
FUHRMAN NUCLEAR GRADE 3, INVASIVE INTO PERINEPHRIC ADIPOSE TISSUE. (SEE COMMENT)
Adrenal gland, no tumor present.
Margins of resection free of tumor.
A solid and partially cystic tumor (10.0 x 9.0 x 6.0 cm) is present in the upper pole of the kidney, extending to the center.
The renal vein is free of tumor. No hilar lymph nodes are identified.