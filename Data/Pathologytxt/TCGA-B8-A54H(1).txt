TCGA-B8-A54H
A: Kidney， left， laparoscopic radical nephrectomy
Tumor ＃1
Histologic tumor type /subtype: renal cell carcinoma，conventional clear cell type
Sarcomatoid features: not identified
Histologic grade （if applicable）: Fuhrman nuclear grade 3 （of 4）
Tumor size （greatest dimension）:7.5 cm
Tumor focality: unifocal, mid and superior pole
Extent of tumor invasion (if present specify if macroscopic or
microscopic) :
Capsular invasion/perirenal adipose tissue: not identified
Gerota' s fascia: not identified
Renal sinus: not identified
Major veins (renal vein or segmental branches, IVC): not
identified
Ureter: not identified
Venous (large vessel): present, microscopic
Lymphatic (small vessel): not identified
Histologic assessment of surgical margins:
Gerota' s fascia (nephrectomy): negative
Renal vein (nephrectomy): negative
Ureter (nephrectomy): negative
Adrenal gland: not submitted
Lymph nodes: no tumor seen in one lymph node (0/1)
Pathologic findings in non-neoplastic kidney:
arteriolonephrosclerosis
AJCC Staging: pT2a pNO
This staging information is based on information available at
the time of this report, and is subject to change pending
clinical review and additional information.

Tumor #2
Histologic tumor type/subtype: papillary renal cell carcinoma,
type 1
Sarcomatoid features: not identified
Histologic grade (if applicable): Fuhrman nuclear grade 2 (of 4)
Tumor size (greatest dimension): 0.8 cm
Tumor focality: unifocal
Extent of tumor invasion (if present specify if macroscopic or
microscopic):
Capsular invasion/perirenal adipose tissue: not identified
Gerota' s fascia: not identified
Renal sinus: not identified
Major veins (renal vein or segmental branches, IVC): not identified
Ureter: not identified
Venous (large vessel): not identified
Lymphatic (small vessel): not identified
Histologic assessment of surgical margins:
Gerota' s fascia (nephrectomy): negative
Renal vein (nephrectomy): negative
Ureter (nephrectomy): negative
Adrenal gland: not submitted
Lymph nodes: no tumor seen in one lymph node (0/1)
Pathologic findings in non-neoplastic kidney: arteriolonephrosclerosis
AJCC Staging pTla pNO
This staging information is based on information available at the time of this
report, and is subject to change pending clinical review and additional information.
