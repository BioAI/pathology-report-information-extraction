TCGA-SX-A7SL

DIAGNOSIS:
A. Left kidney: Papillary renal cell carcinoma, grade 1/4, 4 cm,
confined to the kidney.
Procedure/laterality: Left robotic nephrectomy.
Histologic type: Papillary renal cell carcinoma, type I.
Nuclear grade (Fuhrman, 1-4): Grade 1/4.
Tumor size: 4 x 3.7 x 3.7 cm.
Tumor site: Upper pole left kidney.
Tumor focality: A single circumscribed lesion in the cortex.
Sarcomatoid features: Absent.
Tumor necrosis: Absent.
Microscopic tumor extension:No extension into perinephric tissue,
renal sinus, renal vein or collecting system.
Surgical margins: The external soft tissue and ureteral margins are
negative for carcinoma.
Lymph-vascular invasion: Absent.
Lymph nodes: Absent.
Pathologic findings in nonneoplastic kidney: Adjacent to the
carcinoma there is a 2 cm unilocular renal cyst.
Sections of cortex away from the carcinoma show less than 5%
sclerotic glomeruli, minimal interstitial fibrosis, some dilated
atrophic tubules, and severe renal artery and intra-renal artery
atherosclerosis.
Pathologic staging: pTla, pNX.
Biorepository sample (if applicable): Yes, non-malignant cortex and
carcinoma.
Block(s) containing malignancy suitable for additional testing: Al,
A3, A4, A5.
