TCGA-G7-A4TM
DIAGNOSIS:
A. KIDNEY, RIGHT, PARTIAL NEPHRECTOMY:
- PAPILLARY RENAL CELL CARCINOMA (3.8 X 3.5 X 2.3 CM), TYPE 1
- SURGICAL MARGINS NEGATIVE FOR CARCINOMA
- SEE SYNOPTIC REPORT
SYNOPTIC REPORT - KIDNEY (PARTIAL OR RADICAL)
Specimens Involved
Specimens: A: PARTIAL RIGHT NEPHRECTOMY
Specimen Type: Partial nephrectomy
Without adrenal gland
Laterality: Right
Tumor Site: Not specified
Focality: Unifocal
Tumor Size (largest tumor if multiple): Greatest dimension: 3.8cm
Additional dimensions: 3.5cm x 2.3cm
Macroscopic Extent of Tumor: Tumor limited to kidney
WHO CLASSIFICATION
Papillary renal cell carcinoma 8260/3
Histologic Grade (Fuhrman Nuclear Grade ): G2: Nuclei slightly irregular, approximately 15 u;
nucleoli evident
Invasion of Vascular/Lymphatic: Indeterminate
Perinephric Tissue Invasion : Absent
Margins: Margins uninvolved by invasive carcinoma
Adrenal Gland: Not present
Regional Lymph Nodes: None sampled
Additional Findings : None identified
Pathological Staging (pTNM): pT1a NX MX
Pathological staging is based on the AJCC Cancer Staging Manual, 7th Edition
