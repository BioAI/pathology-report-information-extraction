TCGA-AK-3436
Final Diagnosis:
I. Left kidney and adrenal gland, radical nephrectomy
-Conventional (clear cell) renal carcinoma, Fuhrman grade II (9 cm)
confined to the kidney.
-No angiolymphatic invasion present.
- Vascular, nreter and circumferential margins negative for tumor.
-Adrenal gland negative for tumor.
-Perinephric fat with ectopic adrenal gland.
2. Periaortic lymph node, excision:
-Twenty-two lymph nodes negative for tumor(0/22)
Stage: pT2,NO,Mx.
