TCGA-BP-4991
DIAGNOSIS:
1. SP: Kidney, right upper pole, partial nephrectomy
Tumor Type: Renal cell carcinoma - Conventional {clear cell) type 
Fuhrman Nuclear Grade: Nuclear grade II/IV
Tumor Size: Greatest diameter is 2.5 cm. 
Local Invasion (for renal cortical types): Not Identified 
Renal Vein Invasion: 
  Not Identified
  Small vessel invasion is also not identified 
Surgical Margins: Free of tumor
Non-Neoplastic Kidney: Mild arteriolosclerosis 
Adrenal Gland; Not Identified 
Lymph Nodes: Not identified
Staging for renal cell carcinoma/oncocytoma: pT1 Tumor<= 7.0 cm in greatest dimension limited to the kidney 