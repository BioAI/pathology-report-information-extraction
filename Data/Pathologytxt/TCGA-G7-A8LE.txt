TCGA-G7-A8LE
A. KIDNEY, LEFT, PARTIAL NEPHRECTOMY:
- RENAL CELL CARCINOMA, PAPILLARY TYPE
- FUHRMAN GRADE: 2
- TUMOR SIZE: 4 x 3.8 x 3 CM
- TUMOR LIMITED TO THE KIDNEY
- SURGICAL MARGIN FOCALLY POSITIVE (0.1 CM)
SYNOPTIC REPORT - KIDNEY (PARTIAL OR RADICAL)
Specimens Involved
Specimens : A: LEF TPARTIAL NEPHRECTOMY
Specimen Type: Partial nephrectomy
Without adrenal gland
Laterality: Left
Tumor Site: Not specified
Focality: Unifocal
Tumor Size (largest tumor if multiple): Greatest dimension: 4cm
Additional dimensions: 3.8cm x 3cm
Macroscopic Extent of Tumor: Tumor limited to kidney
WHO CLASSIFICATION
Papillary renal cell carcinoma 8260/3
Histologic Grade (Fuhrman Nuclear Grade): G2: Nuclei slightly irregular, approximately 15 u;
nucleoli evident
Invasion of Vascular/Lymphatic: Absent
Perinephric Tissue Invasion: Absent
Margins : Margin (s) involved by invasive carcinoma
Include: Renal parenchymal margin (partial nephrectomy only)
Distance: 0cm
Adrenal Gland: Not present
Regional Lymph Nodes: None sampled
Additional Findings: Glomerular disease
type:: GLOMERULOSCLEROSIS
Pathological Staging (pTNM): pT1a NX MX
Pathological staging is based on the AJCC Cancer Staging Manual, 7th Edition
pMX: Cannot be assessed
