TCGA-BQ-5885
DIAGNOSIS:
Kidney, left, radical nephrectomy
b.Renal cell carcinoma, clear cell (conventional) type, Fuhrman nuclear grade II/IV
-The tumor measures 2.6cm
-Renal vein invaion: not identified
-Lymph nodes: not identified
-Pathlolgic Stage: pT3
