TCGA-MH-A561
A. LEFT KIDNEY, BASE OF TUMOR, FROZEN SECTION
- BENIGN KIDNEY PARENCHYMA
- NO TUMOR SEEN
B. KIDNEY, LEFT, PARTIAL NEPHRECTOMY
PAPILLARY RENAL CELL CARCINOMA, TYPE 1, GRADE 1
(See cancer case summary and comment)
TUMOR LIMITED TO KIDNEY
Surgical Pathology Cancer Case Summary Based on AJCC /UICC TNM, 7th
edition
- Procedure: Partial nephrectomy
- Specimen Laterality: Left
- Tumor Site: Lower pole
- Tumor Size: 1.8 cm
- Tumor Focality: Unifocal
- Macroscopic Extent of Tumor: Tumor limited to kidney
- Histologic Type: Papillary renal cell carcinoma
- Sarcomatoid Features: Not identified
- Tumor Necrosis: Not identified
- Histologic Grade: Grade 1
- Microscopic Tumor Extension: Tumor limited to kidney
- Margins: Margins uninvolved by invasive carcinoma
- Lymph-Vascular Invasion: Not identified
- Pathologic Staging: pTla, pNx, pMx
- Pathologic Findings in Nonneoplastic Kidney:
- CHRONIC PYELONEPHRITIS

