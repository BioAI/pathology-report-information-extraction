TCGA-BP-5199
DIAGNOSIS:
1. KIDNEY, LEFT AND PROXIMAL URETER; TOTAL NEPHRECTOMY:
Tumor Type: Renal cell carcinoma - Conventional (clear cell) type
  With prominent sarcomatoid features 
Fuhrman Nuclear Grade: Nuclear grade IV/IV 
Tumor Size: Greatest diameter is 7.6 cm.
Local Invasion (for renal cortical types): Not Identified 
Renal Vein Invasion: Not Identified 
Surgical Margins: Free of tumor
Non-Neoplastic Kidney: 
Adrenal Gland: Not identified 
Lymph Nodes: Not identified 
Staging for renal cell carcinoma/oncocytoma: pT2 Tumor >7.0 cm in greatest dimension limited to the kidney