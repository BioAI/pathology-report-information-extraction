TCGA-CZ-5456
PATHOLOGIC DIAGNOSIS:
LEFT KIDNEY, NEPHRECTOMY (1330g) :
RENAL CELL CARCINOMA, clear cell type (12.0 cm) , Fuhrman nuclear grade III/IV,involving the lower pole.
The soft tissue margins are negative for tumor .
The renal artery, vein, and ureter margins are negative for tumor.
The tumor is confined to the k idney .
Hale's colloidal iron is negative in tumor cells. 
The non- ncoplastic kidney will be evaluated by Renal pathology, and thefindings will be reported in an addendum .
AJCC classification (6th Edition) :
pT2 N0 MX.
REGIONAL LYMPH NODES, LYMPH NODE DISSECTION :
Seven lymph nodes, negative for tumor (0:7) .

