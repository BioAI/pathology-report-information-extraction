TCGA-A3-3323
SPECIMEN TYPE: Radical nephrectomy.
LATERALITY: Right.
TUMOR SITE: Middle.
FOCALITY: Unifocal.
TUMOR SIZE (LARGEST TUMOR IF MULTIPLE) :
Greatest dimension: 5.0 cm.
Additional dimensions: 4.5 x 4.0 cm.
MACROSCOPIC EXTENT OF TUMOR: Tumor limited
HISTOLOGIC TYPE: Clear cell (conventional)
HISTOLOGIC GRADE (FUHRMAN NUCLEAR GRADE) :
EXTENT OF INVASION
to kidney.
renal carcinoma.
Grade I.
PRIMARY TUMOR (PT): pT1b: Tumor more than 4.0 cm but not
more than 7.0 cm in greatest dimension, limited to the
kidney.
REGIONAL LYMPH NODES (PN): pNX: Cannot be assessed
DISTANT METASTASIS (PM): pMx: Cannot be assessed
MARGINS: Margins uninvolved by invasive carcinoma.
ADRENAL GLAND: Not present.
VENOUS (LARGE VESSEL) INVASION (V): Absent.
LYMPHATIC (SMALL VESSEL) INVASION (L): Absent.
ADDITIONAL PATHOLOGIC FINDINGS: None identified.
Intradepartmental consultation obtained.
Pathologic stage summary: pT1bNXMXG1
