TCGA-CZ-4863
PATHOLOGIC DIAGNOSIS:
A. SPECIMEN DESIGNATED "LEFT KIDNEY":
RENAL CELL CARCINOMA, clear cell type (7.5 cm), Fuhrman nuclear grade III/IV.
Tumor extends into the perinephric adipose tissue.
Tumor extends into renal vein (to within 2 cm of vein margin).
Renal vein, artery, and ureteric margins are negative for tumor.
AJCC Classification (6th Edition): T3b N0 MX.
The evaluation of this biopsy included PAS and trichrome stains.