TCGA-A3-3307
A: Right kidney
B: Aorto caval lymph nodes
Pathologic Diagnosis
A. Right kidney
Histologic Type: Clear cell (conventional) renal
carcinoma
Fuhrman Grade: 3
Tumor Size: Greatest dimension: 16 cm
Extent of Invasion: Tumor extends into the renal vein and
invades the perinephric fat
Margins: Renal Vein Margin uninvolved by carcinoma
Renal artery margin uninvolved by carcinoma
Ureter margin uninvolved by carcinoma
Distant Metastasis: Cannot be assessed
Specimen Type: Radical nephrectomy
AdrenaJ Gland: Not present
PATHOLOGIC STAGE: pT3b pN0 pMX