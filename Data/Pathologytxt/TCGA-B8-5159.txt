TCGA-B8-5159
Diagnosis:
Kidney, right, laparoscopic radical nephrectomy
Histologic tumor type/subtype: Renal cell carcinoma, clear cell type
Histologic grade (if applicable): 3 (of 4, Fuhrman classification)
Tumor size (greatest dimension): Multifocal (3 nodules), largest 1.9 cm in diameter
Extent of tumor invasion:
Capsular invasion/perirenal adipose tissue: Not identified
Gerota' s fascia: Not received
Renal vein: Not involved
Ureter: Not involved
Venous (large vessel) Not involved
Lymphatic (small vessel): Not involved
Adjacent organs: Not sampled
Histologic assessment of surgical margins:
Perirenal adipose tissue: Negative
Renal vein: Negative
Renal artery: Negative
Ureter: Negative
Adrenal gland: Not received
Lymph nodes: None received
Other significant findings:
- Chronic interstitial nephritis
- Aiieriolonephrosclerosis
AJCC Staging: pTla pNx pMx
This staging information is based on infonnation available at the time of this report, and is subject to change
pending clinical review and additional information. 