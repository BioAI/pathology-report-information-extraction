TCGA-B0-4810
Final diagnosis
'part 1: bone, left 11th rib, excision - .
portion of rib bone with normocellular marrow showing trilineage hematopoiesis;
negative for tumor.
part 2: left kidney, radical nephrectomya.
invasive moderately to poorly differentiated renal cell carcinoma, mixed clear cell
and granular cell types (fuhrman grade 3/4),11.5 cm.
b. Invasive tumor extends to perirenal adipose tissue, renal pelvic adipose tissue, and
invades the proximal renal vein; bl!T does not extend beyond gerota's fascia.
c. Vascular and ureteral margins are negative for tumor.
d. The two of two hilar lymph nodes with metastatic renal cell carcinoma.
e. Left adrenal gland, negative for tumor.
f. Pathologic stage T3 N1 MX; G3.
g. Surrounding kidney parenchyma with massiv~ fat.
part 3: lymph node, retroper\toneum, excision -
four benign reactive lymph nodes, no tumor seen.
part 4: lymph node, perihilar, excisionperipheral
nerve and ga~glia; no lymphoid tissue or tumor seen.
final diagnosis
urine:
satisfactory for interpretation.
atypical urothelial cells in clusters and single cells, suspicious for malignancy
rule out urothelial carcinoma
final diagnosis
bone, fibula, right, biopsy atypical
cells with clear cell and granular cell morphology, consistent with metastatic renal cell carcinoma