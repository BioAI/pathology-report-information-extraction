TCGA-A4-8517
PAPILLARY RENAL CELL CARCINOMA. TYPE 2
Fuhrman nuclear grade 2.
Size: 1.6 cm
Confined to the kidney.
Negative for lymphovascular invasion
Negative for extracapsular invasion into fat
Lymph Nodes:Not evaluated
Pathologic tumor staging descriptors:
Primary tumor（pT）:pT1a.
Regional Lymph nodes（pN）:pNX.
Distant metastasis（pM）:pMX.
Pathologic stage：I.