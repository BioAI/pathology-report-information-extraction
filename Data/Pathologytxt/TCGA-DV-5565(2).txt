TCGA-DV-5565(2)
DIAGNOSIS:
2. "Tumor #2", right kidney (partial nephrectomy): Renal cell carcinoma, clear cell type, Furman nuclear grade 2.
The specimen consists of a red pink and tan fragment of kidney measuring 1.9 x 1.6 x 1 cm.