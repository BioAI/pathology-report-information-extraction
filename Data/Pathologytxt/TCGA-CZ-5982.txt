TCGA-CZ-5982

PATHOLOGIC DIAGNOSIS:
SPECIMEN LABELED "LEFT KIDNEY":
RENAL CELL CARCINOMA, clear cell type (3.7 cm), Fuhrman nuclear grade
II/IV, present in the mid to upper pole.
Tumor is confined to the kidney and does not invade the perinephric fat.
No lymphovascular invasion present.
Ureter, renal artery, and renal vein margins are negative for tumor.
Renal cortical cyst (0.3 cm).
Non-neoplastic kidney will be assessed by the Renal Pathology Service
and reported in an addendum.
AJCC Classification (6th Edition): pT1a Nx MX.
