TCGA-BP-4351
KIDNEY AND ADRENAL, LEFT, DISTAL PANCREAS, SPLEEN; RADICAL NEPHRECTOMY, DISTAL
PANCREATECTOMY, SPLENECTOMY
Tumor Type: Renal cell carcinoma -Conventional (clear cell) type
Fuhrman Nuclear Grade :Nuclear grade II/IV
Tumor Size: Greatest diameter is 16.0 cm.
Local invasion (for renal cortical types): Extends through renal capsule but confined within Gerota's fascia
Renal Vein Invasion: Not identified, Small vessel angiolymphatic invasion is also not identified
Surgical Margins: Free of tumor
Non-Neoplastic Kidney: Mild 3rteriosclerotic changes, and focal interstitial chronic inflammation
Adrenal Gland: Not involved
Lymph Nodes: Free of tumor Number of nodes examinad:1
Staging for renal cell carcinama/oncacytoma: pT3a Tumor invades the adrenal gland or perinephric tissues but not beyond Gerota's fascia
