TCGA-CZ-5451
PATHOLOGIC DIAGNOSIS
A LEFT KIDNEY, RADICAL NEPHRECTOMY
RENAL CELL CARCINOMA, CLEAR CELL TYPE (8.7 cm), Fuhrman grade III, unifocal,
involving the lower pole.
No capsular extension identified.
Soft tissue, ureteral, and vascular margins negative for tumor.
Adrenal gland negative for tumor.
Cortical simple cysts (3.5 cm largest) .
AJCC (:8th edition classification T2 N0 MX
B. SPECIMEN LABELED "10th RIB SEGMENTlI:
Bone, cartilage, and skeletal muscle, negative for tumor.
Maturing trilineage hematopoiesis
C. SPECIMEN LABELED lIREGIONAL NODEll:
Thirteen (13) lymph nodes with negative for tumor.
CLINICAL~
History with weight loss, fatigue
Operation: Nephrectomy. 
Operative Findings: Not provided. .
renal mass by CT.
Clinical Diagnosis: Renal mass by CT.
TISSUE SUBMITTED,
A. (L) kidney
B. 10th rib segment
C. Regional node