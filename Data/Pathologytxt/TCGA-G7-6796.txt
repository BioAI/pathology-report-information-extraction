TCGA-G7-6796
SYNOPTIC REPORT - KIDNEY (PARTIAL OR RADICAL) 
Specimen Type: Partial nephrectomy 
Without adrenal gland 
Laterality: Left 
Tumor Site: Not specified 
Focality: Unifocal 
Tumor Size (largest tumor if multiple): Greatest dimension: 1.5cm 
Macroscopic Extent of Tumor: Tumor limited to kidney 
WHO CLASSIFICATION 
Papillary renal cell carcinoma 8260/3 
Histologic Grade (Fuhrman Nuclear Grade): G2: Nuclei slightly irregular, approximately 15 u; nucleoli evident 
Invasion of Vascular/Lymphatic: Absent 
Perinephric Tissue Invasion: Absent 
Margins: Margins uninvolved by invasive carcinoma 
Adrenal Gland: Not present 
Regional Lymph Nodes: None sampled 
Additional Findings: None identified 
Pathological Staging (pTNM): pT1a NX MX
