TCGA-EV-5903
SPECIMEN TYPE: Left partial nephrectomy.
TUMOR SIZE: 3.5 x 3 x 2 cm.
MACROSCOPIC EXTENT OF TUMOR: Limited to kid ney.
HISTOLOGIC TYPE: Papillary renal cell carcinoma.
HISTOLOGIC GRADE: G2: Nuclei slightly irregular, approximately 15 micron; small nucleoli evident.
EXTENT OF INVASION: T1a: Tumor g cm in greatest dimension, limited to the kidney.
MARGINS: Maigins uninvolved by tumoi.
BLOOD/LYMPHATIC VESSEL INVASION: Not seen.
ADRENAL GLAND: Not present.
REGIONAL LYMPH NODES: NX: Cannot be assessed.
DISTANT METASTASIS: MX: Cannot be assessed.