TCGA-AK-3447

Final Diagnosis:
1. Left kidney, radical nephrectomy:
-Renal cell carcinoma, chromophobe type, Fuhrman nuclear grade 2 (10 cm),
limited to kidney, carcinoma focally invades into but not through the
capsule; see note.
- Ureter and vascular resection margins with no tumor seen.

(pT2NxMx)

