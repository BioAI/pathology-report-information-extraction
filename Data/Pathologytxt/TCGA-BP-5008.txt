TCGA-BP-5008
1. KIDNEY, RIGHT; NEPHRECTOMY:
Tumor Type: Renal cell carcinoma- Conventional (clear cell) type 
Fuhrman Nuclear Grade: Nuclear grade II/IV 
Tumor Size: Greatest diameter Is 2.5 cm. 
Local invasion (for renal cortical types): Not identified 
Renal Vein Invasion: Not Identified 
Surgical Margins: Free of tumor
Non-Neoplastic Kidney: Mild chronic interstitial inflammation and fibrosis 
Adrenal Gland: Not Identified 
Lymph Nodes: Not identified 
Staging for renal cell carcinoma/oncocytoma: pT1 Tumor<= 7.0 cm in greatest dimension limited to the kidney