TCGA-HE-A5NH
Diagnosis Renal cell carcinoma, papillary type (types 1 and 2)
Year of Sample Collection
Age at Sample Collection (yrs)
Days to Procedure Date
Days to Diagnosis
Type of Procedure RESECT
Site of Tissue/Primary (Histology) Left kidney
Tumour Size (cm) 3
Histology Renal cell carcinoma, papillary type (types 1 and 2)
Grade/Differentiation III
Pathological T T1a
Pathological N NX
Clinical M M0
Histology Comments Grade/Diff: Fuhrman grade 3/4.
