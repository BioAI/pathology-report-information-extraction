TCGA-EU-5095

Surgical Pathology Cancer Case Summary, Kidney
SPECIMEN TYPE: Right radical nephrectomy, without adrenal.
TUMOR SITE: Lower pole.
TUMOR SIZE: 3 x 3 x 2.8 cm.
MACROSCOPIC EXTENT OF TUMOR: Limited to kidney.
HISTOLOGIC TYPE: Conventional (clear cell) renal cell carcinoma.
HISTOLOGIC GRADE: G3 (Fuhrman grade 3).
EXTENT OF INVASION: T1.
MARGINS: Uninvolved by tumor.
BLOOD/LYMPHATIC VESSEL INVASION: Not observed.
REGIONAL LYMPH NODES: NX: Cannot be assessed (none included with specimen).
DISTANT METASTASIS: MX: Cannot be assessed.


