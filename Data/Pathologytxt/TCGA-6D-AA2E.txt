TCGA-6D-AA2E
KIDNEY, RIGHT, RADICAL NEPHRECTOMY:
Clear cell renal cell carcinoma, Fuhrman grade 2.
Size of tumor: 6.5 x 5.0 x 3.9 cm.
Location: middle of kidney.
Extent: limited to kidney, no involvement of renal pelvis.
Resection margins:
Vascular margin free of tumor.
Peripheral resection margin free of tumor.
Tumor is 1 from the nearest (perirenal adipose tissue) margin.
Please refer to check list for details.
Pathologic TNM (AJCC 7th edition): pTlb.
Glomerosclerosis is present in a minor component of glomeruli.
Focal fibrosis and focal chronic inflammatory cell infiltration.