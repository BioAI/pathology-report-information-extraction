TCGA-BP-4789
DIAGNOSIS:
1. SP: Kidney, right; partial nephrectomy
Tumor Type:
Renal cell carcinoma - Conventional (clear cell) type
Fuhrman Nuclear Grade:
Nuclear grade II/IV
Tumor Size:
Greatest diameter is 3.4 cm.
Local invasion (for renal cortical types):
Not Identified
Renal Vein Invasion:
Not Identified
Small vessel anglolymphatlc invasion also not Identified.
Surgical Margins:
Free of tumor
Non-Neoplastic Kidney:
Focal hyallnlzlng glomerulosclerosis and mild arteriosclerosis
Adrenal Gland:
Not Identified
Lymph Nodes:
Not Identified
Staging for renal cell carcinoma/oncocytoma:
pT1 Tumor<= 7.0 cm in greatest dimension limited to the kidney 