TCGA-Y8-A8RY

CASE SUMMARY FOR NEPHRECTOMY FOR RENAL CELL CARCINOMA:
Procedure : Laparoscopic nephrectomy
Specimen laterality: Left
Tumor site : Left kidney , central and superior
Tumor size: 4.5 by 4 x 4 cm (central ) and 0.8 cm (superior)
Extent of disease : Limited to the kidney
Histologic type : Papillary renal cell carcinoma, type I
Sarcomatoid features : Not identified
Tumor necrosis: Not identified
Histologic grade ( Fuhrman Nuclear Grade): 2
Margins : Negative for carcinoma
Lymphovascular invasion : Not identified
Pathologic staging ( pTNM):
Primary tumor: pT1 b :
Tumor more than 4 cm but less than 7 cm in greatest dimension
to the kidney
, limited
Regional lymph nodes: pNX: Unable to assess
Distant metastasis: pM: Not applicable
Pathologic findings in nonneoplastic kidney
: None identified
Other tumors and/or tumor -like lesions : Papillary adenomata . Benign renal cyst.
AJCC Staging ( 7th edition )
pT1b pNX pM: Not applicable
