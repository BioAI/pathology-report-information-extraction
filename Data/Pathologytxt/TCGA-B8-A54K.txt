TCGA-B8-A54K
Diagnosis:
A: Kidney, right, partial nephrectomy
Tumor histologic type/subtype: renal cell carcinoma, clear cell
type
Sarcomatoid features: not identified
Histologic grade (if applicable): 1 (of 4, Fuhrman
classification)
Tumor size (greatest dimension): 2.5 cm
Tumor focality: unifocal
Extent of tumor invasion:
Extra-capsular invasion into perirenal adipose tissue: not
identified
Venous: not identified
Lymphatic: not identified
Surgical margins:
Parenchymal margin: negative
Perinephric soft tissue margin: negative
Adrenal gland: n/a
Lymph nodes: n/a
AJCC Stage: pT1a pNx pMx
This staging information is based on information available at
the time of this
report, and is subject to change pending clinical review and
additional
information.
