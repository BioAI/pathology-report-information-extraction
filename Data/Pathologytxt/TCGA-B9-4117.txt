TCGA-B9-4117
Diagnosis :
A: Fat , hilar, NOS , removal
- No tumor seen .
B: Kidney, left , hand assisted laparoscopic nephrectomy
Histologic tumor type/subtype : papillary renal cell carcinoma , type 2
Histologic grade: Fuhrman nuclear grade 3
Tumor size (greatest dimension): 8.4 cm
Extent of tumor invasion:
Capsular invasion/perirenal adipose tissue: negative
Gerota's fascia : negative
Renal vein: negative
Ureter: negati ve
Venous ( large vessel ) : negative
Lymphatic (small vessel) : negative
Adjacent organs : not applicable

Histologic assessment of surgical margins :
Perirenal adipose tissue : negative
Gerota's fascia : negative
Renal vein : negative
Renal artery : negative
Ureter : negative
Adrenal gland : not submitted
Lymph nodes : none identified
AJCC Staging : pT2 pNX pMX